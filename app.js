const service = require('json-server')

const server = service.create();
const apiEndpoints = service.router('./db.json');
const middlewares = service.defaults([noCors=true]);

server.use(middlewares);
server.use(apiEndpoints);

module.exports = server